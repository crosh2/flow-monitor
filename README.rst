Requirements:

1. To have installed pip.
2. To have virtualenv installed.

Getting Started:

1. Clone the project:

	$ git clone https://rolandovillca@bitbucket.org/crosh2/flow-monitor.git

2. Go to project directory:

	$ cd flow-monitor

3. Create virtual environmental directory:

	$ virtualenv VENV

4. Activate the virtual environmental:

	$ . VENV/bin/activate

5. Install all required packages for this project:

	$ pip install -r requirements.txt

6. Check if your packages were installed and version of python you are running:

	$ pip list
	$ which python

7. If you want to deactivate virtual environmental, run following command:

	$ deactivate
